<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Joboffers
 *
 * @ORM\Table(name="JobOffers", indexes={@ORM\Index(name="JobOffers_FK_1", columns={"CategoryId"}), @ORM\Index(name="JobOffers_FK", columns={"ClientId"})})
 * @ORM\Entity(repositoryClass="App\Repository\JoboffersRepository")
 */
class Joboffers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Reference", type="string", length=100, nullable=false)
     */
    private $reference;

    /**
     * @var bool
     *
     * @ORM\Column(name="Activated", type="boolean", nullable=false)
     */
    private $activated;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="JobTitle", type="string", length=100, nullable=true)
     */
    private $jobtitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="JobType", type="string", length=255, nullable=true)
     */
    private $jobtype;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ClosingDate", type="date", nullable=true)
     */
    private $closingdate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="Salary", type="integer", nullable=true)
     */
    private $salary;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CreationDate", type="date", nullable=true)
     */
    private $creationdate;

    /**
     * @var \Clients
     *
     * @ORM\ManyToOne(targetEntity="Clients")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ClientId", referencedColumnName="id")
     * })
     */
    private $clientid;

    /**
     * @var \Categories
     *
     * @ORM\ManyToOne(targetEntity="Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CategoryId", referencedColumnName="id")
     * })
     */
    private $categoryid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Candidats", mappedBy="jobofferid")
     */
    private $candidatid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->candidatid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getActivated(): ?bool
    {
        return $this->activated;
    }

    public function setActivated(bool $activated): self
    {
        $this->activated = $activated;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getJobtitle(): ?string
    {
        return $this->jobtitle;
    }

    public function setJobtitle(?string $jobtitle): self
    {
        $this->jobtitle = $jobtitle;

        return $this;
    }

    public function getJobtype(): ?string
    {
        return $this->jobtype;
    }

    public function setJobtype(?string $jobtype): self
    {
        $this->jobtype = $jobtype;

        return $this;
    }

    public function getClosingdate(): ?\DateTimeInterface
    {
        return $this->closingdate;
    }

    public function setClosingdate(?\DateTimeInterface $closingdate): self
    {
        $this->closingdate = $closingdate;

        return $this;
    }

    public function getSalary(): ?int
    {
        return $this->salary;
    }

    public function setSalary(?int $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getCreationdate(): ?\DateTimeInterface
    {
        return $this->creationdate;
    }

    public function setCreationdate(?\DateTimeInterface $creationdate): self
    {
        $this->creationdate = $creationdate;

        return $this;
    }

    public function getClientid(): ?Clients
    {
        return $this->clientid;
    }

    public function setClientid(?Clients $clientid): self
    {
        $this->clientid = $clientid;

        return $this;
    }

    public function getCategoryid(): ?Categories
    {
        return $this->categoryid;
    }

    public function setCategoryid(?Categories $categoryid): self
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * @return Collection|Candidats[]
     */
    public function getCandidatid(): Collection
    {
        return $this->candidatid;
    }

    public function addCandidatid(Candidats $candidatid): self
    {
        if (!$this->candidatid->contains($candidatid)) {
            $this->candidatid[] = $candidatid;
            $candidatid->addJobofferid($this);
        }

        return $this;
    }

    public function removeCandidatid(Candidats $candidatid): self
    {
        if ($this->candidatid->contains($candidatid)) {
            $this->candidatid->removeElement($candidatid);
            $candidatid->removeJobofferid($this);
        }

        return $this;
    }

}
