<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clients
 *
 * @ORM\Table(name="Clients")
 * @ORM\Entity(repositoryClass="App\Repository\ClientsRepository")
 */
class Clients
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="EntrepriseName", type="string", length=100, nullable=false)
     */
    private $entreprisename;

    /**
     * @var string|null
     *
     * @ORM\Column(name="EntrepriseType", type="string", length=100, nullable=true, options={"default"="NULL"})
     */
    private $entreprisetype = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ContactName", type="string", length=100, nullable=false)
     */
    private $contactname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ContactPosition", type="string", length=100, nullable=true, options={"default"="NULL"})
     */
    private $contactposition = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ContactTel", type="string", length=100, nullable=true, options={"default"="NULL"})
     */
    private $contacttel = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ContactEmail", type="string", length=100, nullable=false)
     */
    private $contactemail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Notes", type="text", length=65535, nullable=true, options={"default"="NULL"})
     */
    private $notes = 'NULL';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntreprisename(): ?string
    {
        return $this->entreprisename;
    }

    public function setEntreprisename(string $entreprisename): self
    {
        $this->entreprisename = $entreprisename;

        return $this;
    }

    public function getEntreprisetype(): ?string
    {
        return $this->entreprisetype;
    }

    public function setEntreprisetype(?string $entreprisetype): self
    {
        $this->entreprisetype = $entreprisetype;

        return $this;
    }

    public function getContactname(): ?string
    {
        return $this->contactname;
    }

    public function setContactname(string $contactname): self
    {
        $this->contactname = $contactname;

        return $this;
    }

    public function getContactposition(): ?string
    {
        return $this->contactposition;
    }

    public function setContactposition(?string $contactposition): self
    {
        $this->contactposition = $contactposition;

        return $this;
    }

    public function getContacttel(): ?string
    {
        return $this->contacttel;
    }

    public function setContacttel(?string $contacttel): self
    {
        $this->contacttel = $contacttel;

        return $this;
    }

    public function getContactemail(): ?string
    {
        return $this->contactemail;
    }

    public function setContactemail(string $contactemail): self
    {
        $this->contactemail = $contactemail;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }


}
