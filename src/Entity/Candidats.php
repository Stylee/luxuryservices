<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Security\Core\User\UserInterface;

use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Candidats
 *
 * @ORM\Table(name="Candidats", indexes={@ORM\Index(name="Candidats_FK", columns={"CategoryId"})})
 * @ORM\Entity(repositoryClass="App\Repository\CandidatsRepository")
 * @Vich\Uploadable
 *
 */
class Candidats implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Gender", type="string", length=255, nullable=true)
     */
    private $gender;

     /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="FirstName", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="LastName", type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Adress", type="string", length=100, nullable=true)
     */
    private $adress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Country", type="string", length=100, nullable=true)
     */
    private $country;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nationality", type="string", length=100, nullable=true)
     */
    private $nationality;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="HasPassport", type="boolean", nullable=true)
     */
    private $haspassport;

    // /**
    //  * @var string|null
    //  *
    //  * @ORM\Column(name="PassportUrl", type="string", length=255, nullable=true)
    //  */
    // private $passporturl;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="candidats_files", fileNameProperty="PassportName", size="passportSize")
     * 
     * @var File
     */
    private $passportFile;

      /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $passportName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $passportSize;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */
    public function setPassportFile(?File $passportFile = null): void
    {
        $this->passportFile = $passportFile;

        if (null !== $passportFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getPassportFile(): ?File
    {
        return $this->passportFile;
    }

    public function setPassportName(?string $passportName): void
    {
        $this->passportName = $passportName;
    }

    public function getPassportName(): ?string
    {
        return $this->passportName;
    }
    
    public function setPassportSize(?int $passportSize): void
    {
        $this->passportSize = $passportSize;
    }

    public function getPassportSize(): ?int
    {
        return $this->passportSize;
    }
    
    // /**
    //  * @var string|null
    //  *
    //  * @ORM\Column(name="CVUrl", type="string", length=255, nullable=true)
    //  */
    // private $cvurl;

 /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="candidats_files", fileNameProperty="cvName", size="cvSize")
     * 
     * @var File
     */
    private $cvFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $cvName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $cvSize;

     /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $cvFile
     */
    public function setCvFile(?File $cvFile = null): void
    {
        $this->cvFile = $cvFile;

        if (null !== $cvFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getCvFile(): ?File
    {
        return $this->cvFile;
    }

    public function setCvName(?string $cvName): void
    {
        $this->cvName = $cvName;
    }

    public function getCvName(): ?string
    {
        return $this->cvName;
    }
    
    public function setCvSize(?int $cvSize): void
    {
        $this->cvSize = $cvSize;
    }

    public function getCvSize(): ?int
    {
        return $this->cvSize;
    }

    // /**
    //  * @var string|null
    //  *
    //  * @ORM\Column(name="ProfilePictureUrl", type="string", length=255, nullable=true)
    //  */
    // private $profilepictureurl;

     /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="candidats_files", fileNameProperty="profilepictureName", size="profilepictureSize")
     * 
     * @var File
     */
    private $profilepictureFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $profilepictureName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $profilepictureSize;

    
/**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $profilepictureFile
     */
    public function setProfilepictureFile(?File $profilepictureFile = null): void
    {
        $this->profilepictureFile = $profilepictureFile;

        if (null !== $profilepictureFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getProfilepictureFile(): ?File
    {
        return $this->profilepictureFile;
    }

    public function setProfilepictureName(?string $profilepictureName): void
    {
        $this->profilepictureName = $profilepictureName;
    }

    public function getProfilepictureName(): ?string
    {
        return $this->profilepictureName;
    }
    
    public function setProfilepictureSize(?int $profilepictureSize): void
    {
        $this->profilepictureSize = $profilepictureSize;
    }

    public function getProfilepictureSize(): ?int
    {
        return $this->profilepictureSize;
    }

    /**
     * @var string|null
     *
     * @ORM\Column(name="CurrentLocation", type="string", length=100, nullable=true)
     */
    private $currentlocation;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="BirthDate", type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BirthPlace", type="string", length=100, nullable=true)
     */
    private $birthplace;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];


    /**
     * @var string The hashed password
     *
     * @ORM\Column(name="Password", type="string", length=100, nullable=false)
     */
    private $password;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="Availability", type="boolean", nullable=true)
     */
    private $availability;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Experience", type="string", length=255, nullable=true)
     */
    private $experience;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CreatedAT", type="date", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DeletedAt", type="date", nullable=true)
     */
    private $deletedat;

    /**
     * @var bool
     *
     * @ORM\Column(name="IsAdmin", type="boolean", nullable=false)
     */
    private $isadmin = 0;

    /**
     * @var \Categories
     *
     * @ORM\ManyToOne(targetEntity="Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CategoryId", referencedColumnName="id")
     * })
     */
    private $categoryid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Joboffers", mappedBy="candidatid")
     */
    private $jobofferid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jobofferid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getHaspassport(): ?bool
    {
        return $this->haspassport;
    }

    public function setHaspassport(?bool $haspassport): self
    {
        $this->haspassport = $haspassport;

        return $this;
    }

    public function getPassporturl(): ?string
    {
        return $this->passporturl;
    }

    public function setPassporturl(?string $passporturl): self
    {
        $this->passporturl = $passporturl;

        return $this;
    }

    public function getCvurl(): ?string
    {
        return $this->cvurl;
    }

    public function setCvurl(?string $cvurl): self
    {
        $this->cvurl = $cvurl;

        return $this;
    }

    public function getProfilepictureurl(): ?string
    {
        return $this->profilepictureurl;
    }

    public function setProfilepictureurl(?string $profilepictureurl): self
    {
        $this->profilepictureurl = $profilepictureurl;

        return $this;
    }

    public function getCurrentlocation(): ?string
    {
        return $this->currentlocation;
    }

    public function setCurrentlocation(?string $currentlocation): self
    {
        $this->currentlocation = $currentlocation;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getBirthplace(): ?string
    {
        return $this->birthplace;
    }

    public function setBirthplace(?string $birthplace): self
    {
        $this->birthplace = $birthplace;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }
    
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAvailability(): ?bool
    {
        return $this->availability;
    }

    public function setAvailability(?bool $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(?string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(?\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getDeletedat(): ?\DateTimeInterface
    {
        return $this->deletedat;
    }

    public function setDeletedat(?\DateTimeInterface $deletedat): self
    {
        $this->deletedat = $deletedat;

        return $this;
    }

    public function getIsadmin(): ?bool
    {
        return $this->isadmin;
    }

    public function setIsadmin(bool $isadmin): self
    {
        $this->isadmin = $isadmin;

        return $this;
    }

    public function getCategoryid(): ?Categories
    {
        return $this->categoryid;
    }

    public function setCategoryid(?Categories $categoryid): self
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * @return Collection|Joboffers[]
     */
    public function getJobofferid(): Collection
    {
        return $this->jobofferid;
    }

    public function addJobofferid(Joboffers $jobofferid): self
    {
        if (!$this->jobofferid->contains($jobofferid)) {
            $this->jobofferid[] = $jobofferid;
            $jobofferid->addCandidatid($this);
        }

        return $this;
    }

    public function removeJobofferid(Joboffers $jobofferid): self
    {
        if ($this->jobofferid->contains($jobofferid)) {
            $this->jobofferid->removeElement($jobofferid);
            $jobofferid->removeCandidatid($this);
        }

        return $this;
    }

    
    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

}
