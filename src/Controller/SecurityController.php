<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\Candidats;
use App\Form\RegistrationType;
use Doctrine\Common\Persistence\ObjectManager;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig',
                             ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/registration", name="security_registration")
     */
    public function registration(
        AuthenticationUtils $authenticationUtils,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        ObjectManager $objectManager
    ): Response
    {
        $candidats = new Candidats;

        $form = $this->createForm(RegistrationType::class, $candidats);
        
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $hashedPassword = $passwordEncoder->encodePassword(
                $candidats,
                $candidats->getPassword());
            $candidats->setPassword($hashedPassword);

            $objectManager->persist($candidats);
            $objectManager->flush();

            return $this->redirectToRoute('security_login');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/register.html.twig',
                             ['last_username' => $lastUsername,
                              'error' => $error,
                              'form' => $form->createView()]);
    }

    
    /**
     * @Route("/logout", name="security_logout")
     */

    public function logout() {
    }
}
