<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190717102920 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Candidats (id INT AUTO_INCREMENT NOT NULL, Gender VARCHAR(255) DEFAULT NULL, FirstName VARCHAR(100) NOT NULL, LastName VARCHAR(100) NOT NULL, Adress VARCHAR(100) DEFAULT NULL, Country VARCHAR(100) DEFAULT NULL, Nationality VARCHAR(100) DEFAULT NULL, HasPassport TINYINT(1) DEFAULT NULL, PassportUrl VARCHAR(255) DEFAULT NULL, CVUrl VARCHAR(255) DEFAULT NULL, ProfilePictureUrl VARCHAR(255) DEFAULT NULL, CurrentLocation VARCHAR(100) DEFAULT NULL, BirthDate DATE DEFAULT NULL, BirthPlace VARCHAR(100) DEFAULT NULL, Email VARCHAR(100) NOT NULL, roles JSON NOT NULL, Password VARCHAR(100) NOT NULL, Availability TINYINT(1) DEFAULT NULL, Experience VARCHAR(255) DEFAULT NULL, Description TEXT DEFAULT NULL, Notes TEXT DEFAULT NULL, CreatedAT DATE DEFAULT NULL, DeletedAt DATE DEFAULT NULL, IsAdmin TINYINT(1) NOT NULL, CategoryId INT DEFAULT NULL, INDEX Candidats_FK (CategoryId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Categories (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Clients (id INT AUTO_INCREMENT NOT NULL, EntrepriseName VARCHAR(100) NOT NULL, EntrepriseType VARCHAR(100) DEFAULT \'NULL\', ContactName VARCHAR(100) NOT NULL, ContactPosition VARCHAR(100) DEFAULT \'NULL\', ContactTel VARCHAR(100) DEFAULT \'NULL\', ContactEmail VARCHAR(100) NOT NULL, Notes TEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE JobOffers (id INT AUTO_INCREMENT NOT NULL, Reference VARCHAR(100) NOT NULL, Activated TINYINT(1) NOT NULL, Notes TEXT DEFAULT NULL, JobTitle VARCHAR(100) DEFAULT NULL, JobType VARCHAR(255) DEFAULT NULL, ClosingDate DATE DEFAULT NULL, Salary INT DEFAULT NULL, CreationDate DATE DEFAULT NULL, ClientId INT DEFAULT NULL, CategoryId INT DEFAULT NULL, INDEX JobOffers_FK_1 (CategoryId), INDEX JobOffers_FK (ClientId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Candidats ADD CONSTRAINT FK_BE97B9B6D36A08A1 FOREIGN KEY (CategoryId) REFERENCES Categories (id)');
        $this->addSql('ALTER TABLE JobOffers ADD CONSTRAINT FK_D47FD25E136A8BE8 FOREIGN KEY (ClientId) REFERENCES Clients (id)');
        $this->addSql('ALTER TABLE JobOffers ADD CONSTRAINT FK_D47FD25ED36A08A1 FOREIGN KEY (CategoryId) REFERENCES Categories (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Candidats DROP FOREIGN KEY FK_BE97B9B6D36A08A1');
        $this->addSql('ALTER TABLE JobOffers DROP FOREIGN KEY FK_D47FD25ED36A08A1');
        $this->addSql('ALTER TABLE JobOffers DROP FOREIGN KEY FK_D47FD25E136A8BE8');
        $this->addSql('DROP TABLE Candidats');
        $this->addSql('DROP TABLE Categories');
        $this->addSql('DROP TABLE Clients');
        $this->addSql('DROP TABLE JobOffers');
    }
}
