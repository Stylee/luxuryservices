-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jul 08, 2019 at 02:10 PM
-- Server version: 10.3.13-MariaDB-1:10.3.13+maria~bionic
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `LuxuryServices`
--

-- --------------------------------------------------------

--
-- Table structure for table `Candidats`
--

CREATE TABLE `Candidats` (
  `id` int(11) NOT NULL,
  `Gender` enum('Female','Male','None') DEFAULT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `Adress` varchar(100) DEFAULT NULL,
  `Country` varchar(100) DEFAULT NULL,
  `Nationality` varchar(100) DEFAULT NULL,
  `HasPassport` tinyint(1) DEFAULT NULL,
  `PassportUrl` varchar(255) DEFAULT NULL,
  `CVUrl` varchar(255) DEFAULT NULL,
  `ProfilePictureUrl` varchar(255) DEFAULT NULL,
  `CurrentLocation` varchar(100) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `BirthPlace` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Availability` tinyint(1) DEFAULT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `Experience` enum('0 - 6 Month','6 month - 1 year','1 - 2 year','2+ years','5+ years','10+ years') DEFAULT NULL,
  `Description` text DEFAULT NULL,
  `Notes` text DEFAULT NULL,
  `CreatedAT` date DEFAULT NULL,
  `DeletedAt` date DEFAULT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Candidatures`
--

CREATE TABLE `Candidatures` (
  `CreatedAt` date NOT NULL,
  `JobOfferId` int(11) NOT NULL,
  `CandidatId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Categories`
--

CREATE TABLE `Categories` (
  `type` varchar(100) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Clients`
--

CREATE TABLE `Clients` (
  `EntrepriseName` varchar(100) NOT NULL,
  `EntrepriseType` varchar(100) DEFAULT NULL,
  `ContactName` varchar(100) NOT NULL,
  `ContactPosition` varchar(100) DEFAULT NULL,
  `ContactTel` varchar(100) DEFAULT NULL,
  `ContactEmail` varchar(100) NOT NULL,
  `Notes` text DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `JobOffers`
--

CREATE TABLE `JobOffers` (
  `Reference` varchar(100) NOT NULL,
  `ClientId` int(11) NOT NULL,
  `Activated` tinyint(1) NOT NULL,
  `Notes` text DEFAULT NULL,
  `JobTitle` varchar(100) DEFAULT NULL,
  `JobType` enum('Fulltime','Part time','Temporary','Freelance','Seasonal') DEFAULT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  `ClosingDate` date DEFAULT NULL,
  `Salary` int(11) DEFAULT NULL,
  `CreationDate` date DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Candidats`
--
ALTER TABLE `Candidats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Candidats_FK` (`CategoryId`);

--
-- Indexes for table `Candidatures`
--
ALTER TABLE `Candidatures`
  ADD PRIMARY KEY (`JobOfferId`,`CandidatId`),
  ADD KEY `Candidatures_FK_1` (`CandidatId`);

--
-- Indexes for table `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Clients`
--
ALTER TABLE `Clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `JobOffers`
--
ALTER TABLE `JobOffers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `JobOffers_FK` (`ClientId`),
  ADD KEY `JobOffers_FK_1` (`CategoryId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Candidats`
--
ALTER TABLE `Candidats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Clients`
--
ALTER TABLE `Clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `JobOffers`
--
ALTER TABLE `JobOffers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Candidats`
--
ALTER TABLE `Candidats`
  ADD CONSTRAINT `Candidats_FK` FOREIGN KEY (`CategoryId`) REFERENCES `Categories` (`id`);

--
-- Constraints for table `Candidatures`
--
ALTER TABLE `Candidatures`
  ADD CONSTRAINT `Candidatures_FK` FOREIGN KEY (`JobOfferId`) REFERENCES `JobOffers` (`id`),
  ADD CONSTRAINT `Candidatures_FK_1` FOREIGN KEY (`CandidatId`) REFERENCES `Candidats` (`id`);

--
-- Constraints for table `JobOffers`
--
ALTER TABLE `JobOffers`
  ADD CONSTRAINT `JobOffers_FK` FOREIGN KEY (`ClientId`) REFERENCES `Clients` (`id`),
  ADD CONSTRAINT `JobOffers_FK_1` FOREIGN KEY (`CategoryId`) REFERENCES `Categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
