* DONE Pour faire fonctionner les assets et le CSS
Ajouter dans  /etc/hosts
=54.38.40.80      kys.idmkr.io=

* DONE Dans un premier temps explorez la maquette
Repérez les formulaires à remplir qui sont au nombre de 5 :
[[file:/var/lib/lxc/1905_devilbox/rootfs/home/devilbox/devilbox/data/www/LuxuryServiceSymfony/Champs formulaires_Candidate.org][file:Champs formulaires_Candidate.org]]

-  le formulaire de création de compte
-  le formulaire de candidature
-  le formulaire de création d'un client (dans l'admin)
-  le formulaire de création d'une offre d'emplois (dans l'admin)
-  le formulaire de contact (optionnel)

* DONE Création du diagramme de la BDD
** DONE Réfléchissez d'abord à la BDD
Vous devrez concevoir le diagramme avec ses relations entre les tables
et le présenter au formateur avant de passer à sa conception.

Vous pourrez ensuite passer par symfony pour la créer ou la faire à la
main dans phpmyadmin.

* Mettre en place une authentification
Vous pouvez utiliser un package pour la gérer ou la faire vous même,
l'authentification comprend la gestion de la table users.

Une grande partie des pages du site ne sera accessible que si les
visiteurs sont authentifiés ou administrateur.

* Donnez vie à vos formulaires
Sur ce projet, nous voulons enregistrer beaucoups de choses dans notre
base de donnée ! attention, certains champs de formulaires des candidats
ne sont visible que par l'admin.

* Dynamisez vos pages
Le contenu de la maquette est dur, il faut le rendre dynamique !

-  la liste des offres d'emplois
-  les données du profil candidat
-  la liste des clients du site
-  la liste des candidats inscrit sur le site
-  les détails sur les offres d'emplois
-  etc

* Ressources
https://zestedesavoir.com/tutoriels/1713/doctrine-2-a-lassaut-de-lorm-phare-de-php/les-relations-avec-doctrine-2/relation-manytoone-et-onetomany-1-n/
