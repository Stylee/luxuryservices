Ce document défini et encadre le projet de création d'un site de
recrutement qui comprendra une base clients, une Cvthèque des candidats
et des candidatures pour les offres d'emploi publiées.

L'identité graphique préalablement définie, sera à intégrer à l'aide des
fichiers Html, Css & Js fournis par le client :
[[https://github.com/Alexandre-Loretzin/Luxury_services_templates][GitHub- Alexandre-Loretzin/Luxury\_services\_templates]]

* Front-office
** VISITEURS
*** Page d'accueil
-  Présentation des offres d'emploi en liste et filtrable par
   catégorie.
- Présence de sections d'appels à l'action pour optimiser la
   conversion de l'utilisateur en candidat.

*** Compagnie
- Présentation de l'histoire et des services de la plateforme de
  recrutement (texte et images).

*** Offres d'emploi
- Présentées en liste et filtrable par catégorie.
- Les offres disponibles ne devront pas afficher le nom du client.
- Seul les utilisateurs inscrits (= candidats) dont le profil est
  complété à 100% pourront postuler aux offres d'emploi.

*** Page contact
 Cette page comportera :

- Les informations générales de contact.
- Un formulaire de contact avec les champs suivants :
    - prénom
    - nom
    - email
    - téléphone
    - message
- Le cabinet de recrutement géolocalisé sur une carte (à intégrer via
  Google Maps API)

*** DONE Inscription
- Le formulaire d'inscription des candidats comportera les champs
  suivants :
   - email
   - mot de passe
   - Le candidat devra accepter les conditions générales d'utilisation de
    la plateforme pour finaliser son inscription (checkbox)

** CANDIDATS
*** Mon profil
 - Cette page est accessible par les candidats inscrits à la plateforme
 - Le profil est sous la forme d'un formulaire dont la complétion est
   affichée en pourcentage.
 - Les candidats devront avoir un profil complété à 100% pour postuler
   à une offre d'emploi.
 - Les champs du profil sont fournis par le client dans le document
   suivant : [[https://docs.google.com/spreadsheets/d/17IH141EIAfFGCSOH48WSkKSEyFPy--2awHtNNzuIRz8/edit?usp=sharing][Champs formulaire candidats]] .
 - Les candidats pourront aussi modifier leur mot de passe sur cette
   page et auront la possibilité de supprimer leurs comptes de la
   plateforme.

*** Connexion / Mot de passe oublié
 - La connexion des candidats se fait avec la paire d'identifiants
   email / mot de passe définis lors de l'inscription.
 - Un formulaire de récupération de mot de passe permet aux candidats
   de recevoir un lien de récupération de compte par email.

* Back-office
L'administrateur se connecte via la même page de connexion que les
candidats mais arrivera sur la page d'accueil de l'administration de la
plateforme

Il n'y a pas de templates fournis pour le back office, a vous de le
créer. Soyez simple et efficaces.

** Pages
*** Dashboard
 Le tableau de bord de l'application présentera des statistiques simples
 telles que :

 -  nombre de candidats
 -  nombre de clients
 -  nombre d'offres d'emploi
 -  nombre de candidatures

*** Candidats
 Les candidats seront présentés en liste ordonnée chronologiquement avec
 les colonnes suivantes :

 -  nom / prénom
 -  email
 -  ville
 -  secteur d'activité
 -  disponibilité
 -  date d'inscription

 L'administrateur pourra :

 -  visualiser le profil détaillé d'un candidat, celui-ci reprendra
    l'ensemble des champs définis dans le document suivant :
    [[https://docs.google.com/spreadsheets/d/17IH141EIAfFGCSOH48WSkKSEyFPy--2awHtNNzuIRz8/edit?usp=sharing][Champs formulaire candidats]]
 -  télécharger les fichiers uploadés du candidat
 -  ajouter une note via un formulaire simple (textarea)
 -  créer un nouveau candidat
 -  supprimer un candidat

*** Clients
 Les clients seront présentés en liste ordonnée chronologiquement avec
 les colonnes suivantes:

 -  nom de la société
 -  nom du contact
 -  email du contact
 -  téléphone du contact
 -  date de création

 L'administrateur pourra :

 -  visualiser le profil détaillé d'un client, celui-ci reprendra
    l'ensemble des champs définis dans le document suivant : [[https://docs.google.com/spreadsheets/d/17IH141EIAfFGCSOH48WSkKSEyFPy--2awHtNNzuIRz8/edit#gid=791347015][Champs  formulaires]]
 -  ajouter une note via un formulaire simple (textarea)
 -  créer un nouveau client
 -  supprimer un client

*** Offres d'emploi
 Les offres d'emploi seront présentées en liste ordonnée
 chronologiquement avec les colonnes suivantes:

 - titre de l'offre
 - nom de la société
 - nom du contact
 - email du contact
 - téléphone du contact
 - statut
 - date de création
 - date de clôture

 L'administrateur pourra :

 - Créer une nouvelle offre d'emploi avec les champs définis dans le
   document suivant : [[https://docs.google.com/spreadsheets/d/17IH141EIAfFGCSOH48WSkKSEyFPy--2awHtNNzuIRz8/edit#gid=2044601402][Champs formulaire offres]]
 - Visualiser la page détaillée d'une offre d'emploi.
 - Visualiser les candidatures en liste ordonnée chronologiquement
   pour l'offre.
 - Ajouter une note via un formulaire simple (textarea).
 - Modifier le statut d'une offre (activée/désactivée).
 - Supprimer une offre d'emploi.

*** Candidatures
 Les candidatures seront présentées en liste ordonnée chronologiquement
 avec les colonnes suivantes:

 - nom du candidat
 - email du candidat
 - titre de l'offre d'emploi
 - nom de la société
 - nom du contact
 - email du contact
 - date du dépôt de candidature

 L'administrateur pourra :

 - Visualiser la page détaillée d'une candidature, celle-ci reprendra
   les champs de l'offre d'emploi et les champs du candidat.
 - Télécharger les fichiers uploadés du candidat.
 - Ajouter une note via un formulaire simple (textarea).
 - Supprimer une candidature.

